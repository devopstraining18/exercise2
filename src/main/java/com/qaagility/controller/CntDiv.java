package com.qaagility.controller;

public class CntDiv {

    public int div(int diva, int divb) {
        if (divb == 0)
        {
            return Integer.MAX_VALUE;
        }
        else
        {
            return diva / divb;
        }
    }

}
