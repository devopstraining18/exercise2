package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;
import com.qaagility.controller.CntDiv;

public class CntDivTest {
    @Test
    public void testCntDivWithZero() throws Exception {

	CntDiv  ab = new CntDiv();
        int div= ab.div(3,0);
       assertEquals("div", Integer.MAX_VALUE, div);
        
    }

   @Test
    public void testCntDiv() throws Exception {

        CntDiv  ab = new CntDiv();
        int div= ab.div(3,1);
       assertEquals("div", 3, div);

    }

}
